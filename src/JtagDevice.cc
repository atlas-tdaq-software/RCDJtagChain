//******************************************************************************
// file: JtagDevice.cc
// desc: device on JTAG chain
// auth: 04/02/05 R. Spiwoks
//******************************************************************************

// $Id$

#include <iostream>
#include <iomanip>

#include "RCDUtilities/RCDUtilities.h"
#include "RCDJtagChain/JtagDevice.h"

using namespace RCD;

//------------------------------------------------------------------------------

JtagDevice::JtagDevice(const std::string& nm, const int is)

    : m_name(nm), m_ir_size(is), m_current_mode(0) {

    // nothing to do
}

//------------------------------------------------------------------------------

JtagDevice::~JtagDevice() {

    // delete all modes
    std::vector<const JtagMode*>::const_iterator ijtgm;
    for(ijtgm = m_list_of_modes.begin(); ijtgm!=m_list_of_modes.end(); ijtgm++) {
	CDBG("deleting mode \"%s\" of device \"%s\"",(*ijtgm)->name().c_str(),m_name.c_str());
	delete (*ijtgm);
    }
}

//------------------------------------------------------------------------------

int JtagDevice::drSize() const {

    if(m_current_mode == (const JtagMode*)0) {
	return(0);
    }
    else {
	return(m_current_mode->drSize());
    }
}


//------------------------------------------------------------------------------

int JtagDevice::addMode(const JtagMode* jtgm) {

    m_list_of_modes.push_back(jtgm);

    return(0);
}

//------------------------------------------------------------------------------

int JtagDevice::setMode(const std::string& jtgm, const BitSet& dr) {

    int		rtnv = -1;

    // find mode by name
    std::vector<const JtagMode*>::const_iterator ijtgm;
    for(ijtgm = m_list_of_modes.begin(); ijtgm!=m_list_of_modes.end(); ijtgm++) {
	if((*ijtgm)->name() == jtgm) {
	    m_current_mode = (*ijtgm);
	    m_dr_data = dr;
	    rtnv = 0;
	    break;
	}
    }

    // not found
    if(rtnv != 0) {
	CERR("mode \"%s\" of device \"%s\" nor found",jtgm.c_str(),m_name.c_str());
    }
    return(rtnv);
}

//------------------------------------------------------------------------------

void JtagDevice::dump() const {

    std::cout << "  JtagDevice \"" << m_name << "\"";
    std::cout << ", IR size = " << std::setw(3) << m_ir_size;
    if(m_current_mode != (JtagMode*)0) {
	std::cout << ", mode = \"" << m_current_mode->name() << "\"";
	std::cout << ", DR data = " << m_dr_data.string() << "";
    }
    else {
	std::cout << ", mode = <none>"; 
    }
    std::cout << ", modes: " << std::endl;

    std::vector<const JtagMode*>::const_iterator ijtgm;
    for(ijtgm = m_list_of_modes.begin(); ijtgm!=m_list_of_modes.end(); ijtgm++) {
	(*ijtgm)->dump();
    }

}
