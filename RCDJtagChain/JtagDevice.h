#ifndef RCDJTAGDEVICE_H
#define RCDJTAGDEVICE_H

//******************************************************************************
// file: JtagDevice.h
// desc: device on JTAG chain
// auth: 04/02/05 R. Spiwoks
//******************************************************************************

// $Id$

#include <string>
#include <vector>

#include "RCDJtagChain/JtagMode.h"

namespace RCD {

class JtagDevice {

  public:
    JtagDevice(const std::string&, const int);
   ~JtagDevice();

    std::string		name() const 		{ return(m_name); }
    int			irSize() const		{ return(m_ir_size); }
    const JtagMode*	currentMode() const	{ return(m_current_mode); }
    BitSet		drData() const		{ return(m_dr_data); }
    int			drSize() const;

    int addMode(const JtagMode*);
    int setMode(const std::string&, const BitSet&);
    int numberOfModes()				{ return(m_list_of_modes.size()); }

    void dump() const;

  private:
    std::string				m_name;
    int					m_ir_size;
    const JtagMode*			m_current_mode;
    BitSet				m_dr_data;
    std::vector<const JtagMode*>	m_list_of_modes;

};	// class JtagDevice

}	// namespace RCD

#endif	// RCDJTAGDEVICE_H
