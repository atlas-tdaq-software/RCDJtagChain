//******************************************************************************
// file: JtagChain.cc
// desc: JTAG chain
// auth: 04/02/05 R. Spiwoks
//******************************************************************************

// $Id$

#include <iostream>
#include <iomanip>

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/sax/HandlerBase.hpp>

#include "RCDUtilities/RCDUtilities.h"
#include "RCDJtagChain/JtagChain.h"

#define TRX(string)	XMLString::transcode(string)

using namespace RCD;
using namespace XERCES_CPP_NAMESPACE;

//------------------------------------------------------------------------------

JtagChain::JtagChain(const std::string& nm, bool xml)

    : m_ir_size(0), m_dr_size(0) {

    // read from XML file
    if(xml) {

	DOMDocument*		document;
	DOMNodeList*		chnList;
	DOMNode*         	chn;
	DOMNodeList*		devList;
	DOMNode*            	dev;
	DOMNodeList*		modList;
	DOMNode*            	mod;
	DOMNode*            	attr0;
	DOMNode*            	attr1;
	DOMNode*            	attr2;
	std::string		nodName;

	bool			errFlag = false;
	int				errCount;

	std::string		devName;
	int			devSize;
	std::string		modName;
	BitSet			modData;
	int			modSize;

	// perform per-process parser initialization
	try {
	    XMLPlatformUtils::Initialize();
	}
	catch(const XMLException& xml_catch) {
	    CERR("during Xerces initialisation, exception = \"%s\"",TRX(xml_catch.getMessage()));
	    return;
	}

	// create a new parser with error handler
	XercesDOMParser*    prs = new XercesDOMParser;
	prs->setValidationScheme(XercesDOMParser::Val_Always);
	ErrorHandler*	err = (ErrorHandler*) new HandlerBase();
	prs->setErrorHandler(err);

	// parse xml file and catch errors
	try {
	    CDBG("XML file = \"%s\"",nm.c_str());
	    prs->parse(nm.c_str());
	    errCount = prs->getErrorCount();
	    if(errCount > 0) {
		errFlag = true;
		CERR("during parsing, errCount = %d",errCount);
	    }
	}
	catch(const XMLException& toCatch) {
	    errFlag = true;
	    CERR("XML exception = \"%s\"",TRX(toCatch.getMessage()));
	}
	catch(const DOMException& toCatch) {
	    errFlag = true;
	    CERR("DOM exception = \"%s\"",TRX(toCatch.msg));
	}
	catch(const SAXParseException& toCatch) {
	    errFlag = true;
	    CERR("SAX Parse exception = \"%s\" in line %d, column %d",TRX(toCatch.getMessage()),toCatch.getLineNumber(),toCatch.getColumnNumber());
	}
	catch(...) {
	    errFlag = true;
	    CERR("other unspecified exception","");
	}
	if(errFlag) {
	    delete prs;
	    delete err;
	    return;
	}

	// get document
	document = prs->getDocument();

	// get JTAG chain
	chnList = document->getChildNodes();
	CDBG("document \"%s\" has %d children",TRX(document->getNodeName()),chnList->getLength());

	// traverse JTAG chain
	for(int i=0; i < (int)chnList->getLength(); i++) {
	    chn  = chnList->item(i);
	    if(chn->getNodeType() != DOMNode::ELEMENT_NODE) continue;
	    nodName = TRX(chn->getNodeName());
	    if(nodName != "JtagChain") {
		CERR("unknown node name \"%s\"",nodName.c_str());
		continue;
	    }
	    attr0 = chn->getAttributes()->getNamedItem(TRX("name"));
	    m_name = TRX(attr0->getNodeValue());

	    // get all JTAG devices
	    devList = chn->getChildNodes();
	    CDBG("chain \"%s\" has %d children",TRX(attr0->getNodeValue()),devList->getLength());

	    // traverse all JTAG devices
	    for(int l=0; l<(int)devList->getLength(); l++) {
		dev = devList->item(l);
		if(dev->getNodeType() != DOMNode::ELEMENT_NODE) continue;
		attr0 = dev->getAttributes()->getNamedItem(TRX("name"));
		attr1 = dev->getAttributes()->getNamedItem(TRX("ir_size"));
		devName = TRX(attr0->getNodeValue()); 
		devSize = atoi(TRX(attr1->getNodeValue()));
		CDBG("found device \"%s\" with size = %d",devName.c_str(),devSize);

		// add new device
		addDevice(devName,devSize);

		// get all JTAG modes
		modList = dev->getChildNodes();
		CDBG("device \"%s\" has %d children",TRX(attr0->getNodeValue()),modList->getLength());

		// traverse all JTAG modes
		for(int m=0; m<(int)modList->getLength(); m++) {
		    mod = modList->item(m);
		    if(mod->getNodeType()!=DOMNode::ELEMENT_NODE) continue;
		    attr0 = mod->getAttributes()->getNamedItem(TRX("name"));
		    attr1 = mod->getAttributes()->getNamedItem(TRX("ir_data"));
		    attr2 = mod->getAttributes()->getNamedItem(TRX("dr_size"));
		    modName = TRX(attr0->getNodeValue());
		    modData = BitSet(TRX(attr1->getNodeValue()));
		    modSize = atoi(TRX(attr2->getNodeValue()));
		    CDBG("found mode \"%s\" with data \"%s\" and size %d",modName.c_str(),modData.string().c_str(),modSize);

		    // add new mode
		    addDeviceMode(devName,modName,modData,modSize);
		}
	    }
	}

	// clean up
	delete prs;
	delete err;
    }
    // instantiate with name
    else {
	m_name = nm;
    }
}

//------------------------------------------------------------------------------

JtagChain::~JtagChain() {

    // delete all devices
    std::vector<JtagDevice*>::iterator ijtgd;
    for(ijtgd = m_list_of_devices.begin(); ijtgd!=m_list_of_devices.end(); ijtgd++) {
	CDBG("deleting device \"%s\" of chain \"%s\"",(*ijtgd)->name().c_str(),m_name.c_str());
	delete (*ijtgd);
    }
}

//------------------------------------------------------------------------------

int JtagChain::addDevice(const std::string& nm, const int il) {

    // create new device
    JtagDevice*	jtgd = new JtagDevice(nm,il);

    // add device to list and update IR size
    m_list_of_devices.push_back(jtgd);
    m_ir_size += jtgd->irSize();

    return(0);
}

//------------------------------------------------------------------------------

int JtagChain::addDeviceMode(const std::string& dv, const std::string& nm, const BitSet& dr, const int dl) {

    int		rtnv = -1;
    JtagDevice*	rjtgd = (JtagDevice*)0;

    // find device by name
    std::vector<JtagDevice*>::iterator ijtgd;
    for(ijtgd = m_list_of_devices.begin(); ijtgd!=m_list_of_devices.end(); ijtgd++) {
        if((*ijtgd)->name() == dv) {
	    rjtgd = (*ijtgd);
            rtnv = 0;
            break;
        }
    }
    // not found
    if(rtnv != 0) {
        CERR("device \"%s\" not found",dv.c_str());
	return(rtnv);
    }

    // create new mode and add to device
    JtagMode*	jtgm = new JtagMode(nm,dr,dl);
    rjtgd->addMode(jtgm);

    return(0);
}

//------------------------------------------------------------------------------

int JtagChain::numberOfDevices() {

    return(m_list_of_devices.size());
}

//------------------------------------------------------------------------------

std::string JtagChain::nameOfDevice(const int jtgd) const {

    // check device index
    if(jtgd >= (int)m_list_of_devices.size()) {
        CERR("asking for device #%d in chain which only contains %d devices",jtgd,(int)m_list_of_devices.size());
	return("");
    }

    return(m_list_of_devices[jtgd]->name());
}

//------------------------------------------------------------------------------

RCD::BitSet JtagChain::getDrData(const int jtgd) const {

    // check device index
    if(jtgd >= (int)m_list_of_devices.size()) {
        CERR("asking for device #%d in chain which only contains %d devices",jtgd,(int)m_list_of_devices.size());
	return(RCD::BitSet(""));
    }

    return(m_list_of_devices[jtgd]->drData());
}

//------------------------------------------------------------------------------

int JtagChain::setDeviceMode(const std::string& jtgd, const std::string& jtgm, const BitSet& dr) {

    int		rtnv = -1;
    JtagDevice*	rjtgd = (JtagDevice*)0;

    // find device by name
    std::vector<JtagDevice*>::iterator ijtgd;
    for(ijtgd = m_list_of_devices.begin(); ijtgd!=m_list_of_devices.end(); ijtgd++) {
        if((*ijtgd)->name() == jtgd) {
	    rjtgd = (*ijtgd);
            rtnv = 0;
            break;
        }
    }
    // not found
    if(rtnv != 0) {
        CERR("device \"%s\" not found",jtgd.c_str());
	return(rtnv);
    }

    // set device mode
    int save = rjtgd->drSize();
    if((rtnv = rjtgd->setMode(jtgm,dr)) != 0) {
	CERR("setting device \"%s\" to mode \"%s\"",rjtgd->name().c_str(),jtgm.c_str());
	return(rtnv);
    }
    m_dr_size -= save;
    m_dr_size += rjtgd->drSize();
    return(0);
}

//------------------------------------------------------------------------------

int JtagChain::setDeviceMode(const int jtgd, const std::string& jtgm, const BitSet& dr) {

    int		rtnv = -1;

    // check device index
    if(jtgd >= (int)m_list_of_devices.size()) {
        CERR("asking for device #%d in chain which only contains %d devices",jtgd,(int)m_list_of_devices.size());
	return(rtnv);
    }

    // set device mode
    JtagDevice*	rjtgd = m_list_of_devices[jtgd];
    int save = rjtgd->drSize();
    if((rtnv = rjtgd->setMode(jtgm,dr)) != 0) {
	CERR("setting device \"%s\" to mode \"%s\"",rjtgd->name().c_str(),jtgm.c_str());
	return(rtnv);
    }
    m_dr_size -= save;
    m_dr_size += rjtgd->drSize();
    return(0);
}

//------------------------------------------------------------------------------

int JtagChain::numberOfDeviceModes(const std::string& jtgd) {

    int		rtnv = -1;
    JtagDevice*	rjtgd = (JtagDevice*)0;

    // find device by name
    std::vector<JtagDevice*>::iterator ijtgd;
    for(ijtgd = m_list_of_devices.begin(); ijtgd!=m_list_of_devices.end(); ijtgd++) {
        if((*ijtgd)->name() == jtgd) {
	    rjtgd = (*ijtgd);
            rtnv = 0;
            break;
        }
    }
    // not found
    if(rtnv != 0) {
        CERR("device \"%s\" not found",jtgd.c_str());
	return(-1);
    }

    return(rjtgd->numberOfModes());
}

//------------------------------------------------------------------------------

int JtagChain::numberOfDeviceModes(const int jtgd) {

    int		rtnv = -1;

    // check device index
    if(jtgd >= (int)m_list_of_devices.size()) {
        CERR("asking for device #%d in chain which only contains %d devices",jtgd,(int)m_list_of_devices.size());
	return(rtnv);
    }

    return(m_list_of_devices[jtgd]->numberOfModes());
}

//------------------------------------------------------------------------------

int JtagChain::ir_tdo(BitSet& bs) const {

    int			ipos = 0;
    BitSet		rslt(m_ir_size);

    // iterate over all current JTAG modes
    std::vector<JtagDevice*>::const_iterator ijtgd;
    for(ijtgd=m_list_of_devices.begin(); ijtgd!=m_list_of_devices.end(); ijtgd++) {
	if((*ijtgd)->currentMode() == (const JtagMode*)0) {
	    CERR("device \"%s\" has no mode",(*ijtgd)->name().c_str());
	    return(-1);
	}
	for(int i=0; i<(*ijtgd)->irSize(); i++) {
	    CDBG("ipos = %3d, i= %3d, rslt = %s",ipos,i,rslt.string().c_str());
	    rslt[ipos] = (*ijtgd)->currentMode()->irData()[i];
	    ipos++;
	}
    }

    bs = rslt;
    return(0);
}

//------------------------------------------------------------------------------

int JtagChain::dr_tdo(BitSet& bs) const {

    int			ipos = 0;
    BitSet		rslt(m_dr_size);

    // iterate over all current JTAG DR data
    std::vector<JtagDevice*>::const_iterator ijtgd;
    for(ijtgd=m_list_of_devices.begin(); ijtgd!=m_list_of_devices.end(); ijtgd++) {
	if((*ijtgd)->currentMode() == (const JtagMode*)0) {
	    CERR("device \"%s\" has no mode",(*ijtgd)->name().c_str());
	    return(-1);
	}
	for(int i=0; i<(*ijtgd)->drSize(); i++) {
	    CDBG("ipos = %3d, i= %3d, rslt = %s",ipos,i,rslt.string().c_str());
	    rslt[ipos] = (*ijtgd)->drData()[i];
	    ipos++;
	}
    }

    bs = rslt;
    return(0);
}

//------------------------------------------------------------------------------

int JtagChain::ir_tdi(const std::string& jtgd, const BitSet& bsi, BitSet& bso) const {

    int			rtnv = -1;
    JtagDevice*		rjtgd = (JtagDevice*)0;
    int			ipos = 0;

    // find device by name
    std::vector<JtagDevice*>::const_iterator ijtgd;
    for(ijtgd = m_list_of_devices.begin(); ijtgd!=m_list_of_devices.end(); ijtgd++) {
        if((*ijtgd)->name() == jtgd) {
	    rjtgd = (*ijtgd);
            rtnv = 0;
            break;
        }
	ipos += (*ijtgd)->irSize();
    }
    // not found
    if(rtnv != 0) {
        CERR("device \"%s\" not found",jtgd.c_str());
	return(rtnv);
    }

    // check size of input bit set
    if((int)bsi.size() < (ipos+rjtgd->irSize())) {
	CERR("input IR_TDI size (%d) smaller than size/position for device \"%s\" (%d)",bsi.size(),rjtgd->name().c_str(),ipos+rjtgd->irSize());
	return(-1);
    }

    // iterate over current JTAG mode
    BitSet rslt(rjtgd->irSize());
    for(int i=0; i<rjtgd->irSize(); i++) {
	CDBG("i = %3d, ipos = %3d, rslt = %s",i,ipos+i,rslt.string().c_str());
	rslt[i] = bsi[ipos+i];
    }

    bso = rslt;
    return(0);
}

//------------------------------------------------------------------------------

int JtagChain::ir_tdi(const int jtgd, const BitSet& bsi, BitSet& bso) const {

    int			rtnv = -1;
    int			ipos = 0;

    // check device index
    if(jtgd >= (int)m_list_of_devices.size()) {
        CERR("asking for device #%d in chain which only contains %d devices",jtgd,(int)m_list_of_devices.size());
	return(rtnv);
    }

    // obtain device
    for(int i=0; i<jtgd; i++) {
	ipos += m_list_of_devices[i]->irSize();
    }
    JtagDevice*	rjtgd = m_list_of_devices[jtgd];

    // check size of input bit set
    if((int)bsi.size() < (ipos+rjtgd->irSize())) {
	CERR("input IR_TDI size (%d) smaller than size/position for device \"%s\" (%d)",bsi.size(),rjtgd->name().c_str(),ipos+rjtgd->irSize());
	return(-1);
    }

    // iterate over current JTAG mode
    BitSet rslt(rjtgd->irSize());
    for(int i=0; i<rjtgd->irSize(); i++) {
	CDBG("i = %3d, ipos = %3d, rslt = %s",i,ipos+i,rslt.string().c_str());
	rslt[i] = bsi[ipos+i];
    }

    bso = rslt;
    return(0);
}

//------------------------------------------------------------------------------

int JtagChain::dr_tdi(const std::string& jtgd, const BitSet& bsi, BitSet& bso) const {

    int			rtnv = -1;
    JtagDevice*		rjtgd = (JtagDevice*)0;
    int			ipos = 0;

    // find device by name
    std::vector<JtagDevice*>::const_iterator ijtgd;
    for(ijtgd = m_list_of_devices.begin(); ijtgd!=m_list_of_devices.end(); ijtgd++) {
        if((*ijtgd)->name() == jtgd) {
	    rjtgd = (*ijtgd);
            rtnv = 0;
            break;
        }
	ipos += (*ijtgd)->drSize();
    }
    // not found
    if(rtnv != 0) {
        CERR("device \"%s\" not found",jtgd.c_str());
	return(rtnv);
    }

    // check size of input bit set
    if((int)bsi.size() < (ipos+rjtgd->drSize())) {
	CERR("input DR_TDI size (%d) smaller than size/position for device \"%s\" (%d)",bsi.size(),rjtgd->name().c_str(),ipos+rjtgd->drSize());
	return(-1);
    }

    // iterate over current JTAG mode
    BitSet rslt(rjtgd->drSize());
    for(int i=0; i<rjtgd->drSize(); i++) {
	CDBG("i = %3d, ipos = %3d, rslt = %s",i,ipos+i,rslt.string().c_str());
	rslt[i] = bsi[ipos+i];
    }

    bso = rslt;
    return(0);
}

//------------------------------------------------------------------------------

int JtagChain::dr_tdi(const int jtgd, const BitSet& bsi, BitSet& bso) const {

    int			rtnv = -1;
    int			ipos = 0;

    // check device index
    if(jtgd >= (int)m_list_of_devices.size()) {
        CERR("asking for device #%d in chain which only contains %d devices",jtgd,(int)m_list_of_devices.size());
	return(rtnv);
    }

    // obtain device
    for(int i=0; i<jtgd; i++) {
	ipos += m_list_of_devices[i]->drSize();
    }
    JtagDevice*	rjtgd = m_list_of_devices[jtgd];

    // check size of input bit set
    if((int)bsi.size() < (ipos+rjtgd->drSize())) {
	CERR("input DR_TDI size (%d) smaller than size/position for device \"%s\" (%d)",bsi.size(),rjtgd->name().c_str(),ipos+rjtgd->drSize());
	return(-1);
    }

    // iterate over current JTAG mode
    BitSet rslt(rjtgd->drSize());
    for(int i=0; i<rjtgd->drSize(); i++) {
	CDBG("i = %3d, ipos = %3d, rslt = %s",i,ipos+i,rslt.string().c_str());
	rslt[i] = bsi[ipos+i];
    }

    bso = rslt;
    return(0);
}

//------------------------------------------------------------------------------

void JtagChain::dump() const {

    std::cout << "JtagChain \"" << m_name << "\":" << std::endl;

    std::vector<JtagDevice*>::const_iterator ijtgd;
    for(ijtgd = m_list_of_devices.begin(); ijtgd!=m_list_of_devices.end(); ijtgd++) {
	(*ijtgd)->dump();
    }
}
