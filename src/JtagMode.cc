//******************************************************************************
// file: JtagMode.cc
// desc: operation mode of device on JTAG chain
// auth: 04/02/05 R. Spiwoks
//******************************************************************************

// $Id$

#include <iostream>
#include <iomanip>

#include "RCDUtilities/RCDUtilities.h"
#include "RCDJtagChain/JtagMode.h"

using namespace RCD;

//------------------------------------------------------------------------------

JtagMode::JtagMode(const std::string& nm, const BitSet& ir, const int ds)

    : m_name(nm), m_ir_data(ir), m_dr_size(ds) {

    // nothing to do
}

//------------------------------------------------------------------------------

JtagMode::~JtagMode() {

    // nothing to do
}

//------------------------------------------------------------------------------

void JtagMode::dump() const {

    // nothing to do
    std::cout << "    JtagMode \"" << m_name << "\"";
    std::cout << ": IR data = " << m_ir_data.string();
    std::cout << ", DR size = " << std::setw(3) << m_dr_size << std::endl;
}
