//******************************************************************************
// file: testRCDJtagChain.cc
// desc: test program for JTAG chain
// auth: 04/02/05 R. Spiwoks
//******************************************************************************

// $Id$

#include <iostream>
#include <iomanip>

#include "RCDJtagChain/JtagChain.h"

using namespace RCD;

//------------------------------------------------------------------------------

int main(int /* argc */, char* /* argv */[]) {

    // create JTAG chain
    JtagChain		chain0("CHAIN0",false);
    chain0.addDevice("DEVICE0",7);
    chain0.addDeviceMode("DEVICE0","BYPASS",BitSet("0xff"),1);
    chain0.addDeviceMode("DEVICE0","IDCODE",BitSet("0x01"),32);
    chain0.addDevice("DEVICE1",7);
    chain0.addDeviceMode("DEVICE1","BYPASS",BitSet("0xff"),1);
    chain0.addDeviceMode("DEVICE1","IDCODE",BitSet("0x02"),32);
    chain0.addDevice("DEVICE2",7);
    chain0.addDeviceMode("DEVICE2","BYPASS",BitSet("0xff"),1);
    chain0.addDeviceMode("DEVICE2","IDCODE",BitSet("0x03"),32);
    chain0.addDevice("DEVICE3",7);
    chain0.addDeviceMode("DEVICE3","BYPASS",BitSet("0xff"),1);
    chain0.addDeviceMode("DEVICE3","IDCODE",BitSet("0x44"),32);
    chain0.addDevice("DEVICE4",7);
    chain0.addDeviceMode("DEVICE4","BYPASS",BitSet("0xff"),1);
    chain0.addDeviceMode("DEVICE4","IDCODE",BitSet("0x55"),32);

    // set JTAG chain modes
    chain0.setDeviceMode("DEVICE0","BYPASS",BitSet("0x1"));
    chain0.setDeviceMode("DEVICE1","IDCODE",BitSet("0x0"));
    chain0.setDeviceMode("DEVICE2","IDCODE",BitSet("0x1"));
    chain0.setDeviceMode(3,"BYPASS",BitSet("0x1"));
    chain0.setDeviceMode(4,"BYPASS",BitSet("0x1"));

    // dump JTAG chain
    chain0.dump();

    // obtain IR data
    BitSet	ir_tdo;
    chain0.ir_tdo(ir_tdo);
    std::cout << "ir_tdo = " << ir_tdo.string() << ", size = " << ir_tdo.size() << std::endl;

    // obtain DR data
    BitSet	dr_tdo;
    chain0.dr_tdo(dr_tdo);
    std::cout << "dr_tdo = " << dr_tdo.string() << ", size = " << dr_tdo.size() << std::endl;

    // obtain IR data for single device
    BitSet	ir_tdi;
    chain0.ir_tdi("DEVICE1",ir_tdo,ir_tdi);
    std::cout << "ir_tdi(\"DEVICE1\") = " << ir_tdi.string() << ", size = " << ir_tdi.size() << std::endl;
    
    // obtain DR data for single device
    BitSet	dr_tdi;
    chain0.dr_tdi("DEVICE1",dr_tdo,dr_tdi);
    std::cout << "dr_tdi(\"DEVICE1\") = " << dr_tdi.string() << ", size = " << dr_tdi.size() << std::endl;

// -----------------------------------------------------------------------------

    // read chain from XML file
    JtagChain	chain1("../data/test.xml");

    // set JTAG chain modes
    chain1.setDeviceMode("DEVICE0","BYPASS",BitSet("0x1"));
    chain1.setDeviceMode("DEVICE1","IDCODE",BitSet("0x0"));
    chain1.setDeviceMode("DEVICE2","BYPASS",BitSet("0x1"));
    chain1.setDeviceMode("DEVICE3","BYPASS",BitSet("0x1"));
    chain1.setDeviceMode("DEVICE4","BYPASS",BitSet("0x1"));

    // dump JTAG chain
    chain1.dump();

    // obtain IR data for single device
    chain1.ir_tdi(1,ir_tdo,ir_tdi);
    std::cout << "ir_tdi(#1) = " << ir_tdi.string() << ", size = " << ir_tdi.size() << std::endl;
    
    // obtain DR data for single device
    chain1.dr_tdi(1,dr_tdo,dr_tdi);
    std::cout << "dr_tdi(#1) = " << dr_tdi.string() << ", size = " << dr_tdi.size() << std::endl;

    
}
