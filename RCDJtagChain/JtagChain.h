#ifndef RCDJTAGCHAIN_H
#define RCDJTAGCHAIN_H

//******************************************************************************
// file: JtagChain.h
// desc: JTAG chain
// auth: 04/02/05 R. Spiwoks
//******************************************************************************

// $Id$

#include <string>
#include <vector>

#include "RCDJtagChain/JtagDevice.h"

namespace RCD {

class JtagChain {

  public:
    JtagChain(const std::string&, bool = true);
   ~JtagChain();

    int addDevice(const std::string&, const int);
    int addDeviceMode(const std::string&, const std::string&, const BitSet&, const int);
    int numberOfDevices();

    std::string nameOfDevice(const int) const;
    int setDeviceMode(const std::string&, const std::string&, const BitSet&);
    int setDeviceMode(const int         , const std::string&, const BitSet&);
    RCD::BitSet getDrData(const int) const;
    int numberOfDeviceModes(const std::string&);
    int numberOfDeviceModes(const int);

    int ir_tdo(BitSet&) const;
    int dr_tdo(BitSet&) const;
    int ir_tdi(const std::string&, const BitSet&, BitSet&) const;
    int ir_tdi(const int         , const BitSet&, BitSet&) const;
    int dr_tdi(const std::string&, const BitSet&, BitSet&) const;
    int dr_tdi(const int         , const BitSet&, BitSet&) const;

    void dump() const;

  private:
    std::string			m_name;
    std::vector<JtagDevice*>	m_list_of_devices;
    int				m_ir_size;
    int				m_dr_size;

};	// class JtagChain

}	// namespace RCD

#endif	// RCDJTAGCHAIN_H
