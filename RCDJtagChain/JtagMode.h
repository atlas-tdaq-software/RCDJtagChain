#ifndef RCDJTAGMODE_H
#define RCDJTAGMODE_H

//******************************************************************************
// file: JtagMode.h
// desc: operation mode of device on JTAG chain
// auth: 04/02/05 R. Spiwoks
//******************************************************************************

// $Id$

#include <string>

#include "RCDBitString/BitSet.h"

namespace RCD {

class JtagMode {

  public:
    JtagMode(const std::string&, const BitSet&, const int);
   ~JtagMode();

    std::string name() const	{ return(m_name); };
    BitSet      irData() const	{ return(m_ir_data); };
    int         drSize() const	{ return(m_dr_size); };

    void dump() const;

  private:
    std::string			m_name;
    BitSet			m_ir_data;
    int				m_dr_size;

};	// class JtagMode

}	// namespace RCD

#endif	// RCDJTAGMODE_H
